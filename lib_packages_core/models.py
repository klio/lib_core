# coding=utf-8
import os

from django.db import models


class NoDeletedObjectsManager(models.Manager):
    u"""
    Манаджер для моделей, объекты которых не удаляются из системы.
    """
    use_for_related_fields = True

    def get_query_set(self):
        return super(NoDeletedObjectsManager, self).get_query_set().filter(
            deleted=False,
        )


class NoDeletedModel(models.Model):
    u""" Базовый класс для моделей, где записи физически не удаляются
    """
    deleted = models.BooleanField(
        verbose_name=u'Признак удаления записи',
        default=False,
    )

    objects = NoDeletedObjectsManager()
    objects_all = models.Manager()

    def delete(self, using=None, keep_parents=False):
        u"""Прямое удаление отсутствует, только пометка на удаление.
        """
        self.deleted = True
        self.save()

    class Meta:
        abstract = True


class LogModifiedModel(models.Model):
    u""" Базовый класс для моделей с хронологией создания и изменения
    """
    created = models.DateTimeField(
        verbose_name=u'Момент создания',
        help_text=u'Момент создания',
        auto_now_add=True,
        null=True, blank=True,
    )
    modified = models.DateTimeField(
        verbose_name=u'Момент изменения',
        help_text=u'Момент изменения',
        auto_now=True,
        null=True, blank=True,
    )

    class Meta:
        abstract = True


class CommandTasks(models.Model):
    u""" реестр запускаемых процессов """
    command = models.CharField(
        verbose_name=u"Имя процесса",
        max_length=255
    )
    pid = models.IntegerField(
        verbose_name=u"id процесса в системе",
        default=os.getpid
    )
    modified = models.DateTimeField(
        verbose_name=u"Время последнего запуска",
        auto_now=True,
    )

    class Meta:
        db_table = 'core_command_tasks'

# coding=utf-8
import os


def remove_dir_rec(path_dir):
    u""" удаляет все содержимое директории. пустая директория тоже удаляется """
    dir_list = []
    if os.path.exists(path_dir):
        for top, dirs, files in os.walk(path_dir):
            # файлы удаляем
            for f in files:
                path_file = os.path.join(top, f)
                os.remove(path_file)
            # папки собираем
            for d in dirs:
                path_dir = os.path.join(top, d)
                dir_list.append(path_dir)

    # папки должны быть уже пустые
    for d in sorted(dir_list, reverse=True):
        if os.path.exists(d):
            os.removedirs(d)


def create_path_dir(path_dir):
    u""" Создает структуру директорий """
    if not os.path.exists(path_dir):
        create_path_dir(os.path.dirname(path_dir))
        os.mkdir(path_dir)

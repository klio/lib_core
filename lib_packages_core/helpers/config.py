# coding=utf-8
import os
import yaml

from dicts import DotDict


conf = DotDict()


class _Conf(object):

    __loaded = False

    @classmethod
    def get_config(cls, base_dir=None):
        u""" загружаем конфиг один раз

        """
        if cls.__loaded:
            return conf

        # при загрузке базовый каталог должен быть задан
        assert base_dir is not None

        with open(os.path.join(base_dir, 'configs', 'config.yml'), 'r') as f:
            cls.update_conf(conf, yaml.load(f))

        # локальный конфиг (если есть)
        try:
            with open(os.path.join(base_dir, 'configs', 'config.local.yml'), 'r') as f:
                cls.update_conf(conf, yaml.load(f))
        except IOError:
            pass

        return conf

    @classmethod
    def update_conf(cls, conf_, data):
        if data is None:
            return
        for key, val in data.iteritems():
            if isinstance(val, dict):
                cls.update_conf(conf_[key], val)
            else:
                conf_[key] = val

get_config = _Conf.get_config

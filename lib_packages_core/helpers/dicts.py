# coding=utf-8
from collections import defaultdict


def rec_dict():
    u""" Рекурсивный словарь
    """
    return defaultdict(lambda: defaultdict(rec_dict))


class DotDict(object):
    u""" Рекурсивный словарь с точками
    """
    def __init__(self):
        self.__val = defaultdict(lambda: DotDict())

    def __getattr__(self, key):
        return self.__getitem__(key) if key in self._DotDict__val else None

    def __setitem__(self, key, value):
        if self.__val is None:
            self.__val = {}
        self.__val[key] = value

    def __getitem__(self, key):
        return self.__val[key]

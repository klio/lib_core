# coding:utf-8


def json_encode(f):
    u""" Декоратор, которым нужно отмечать сериализуемые в JSONEncoder методы """
    f.json_encode = True
    return f

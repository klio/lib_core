# coding=utf-8
import re


def str2number(str_number):
    u""" Из произвольной строки делает число. Все что не цифры отбросится """
    if isinstance(str_number, int):
        return str_number
    if not isinstance(str_number, basestring):
        return 0
    if isinstance(str_number, unicode):
        str_number = str_number.encode('utf-8')
    str_number = re.sub(r'[^0-9]', '', str_number)
    return int(str_number) if str_number else 0

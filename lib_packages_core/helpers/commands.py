# coding=utf-8
import functools
import os
from subprocess import Popen, PIPE

from log import log_error, log_info
from ..models import CommandTasks


def run_is_not_running(func):
    u""" предотвращает повторный запуск процесса """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):

        command_module = func.__module__.split('.')[-1]

        try:
            task = CommandTasks.objects.get(command=command_module)
        except CommandTasks.DoesNotExist:
            task = CommandTasks.objects.create(command=command_module, pid=os.getpid())
        else:
            ps = Popen(
                'ps aux | grep " %d " | grep "%s" | grep -v grep' % (task.pid, command_module),
                stdout=PIPE, shell=True).stdout.read().strip()
            if ps:
                log_info(u"Процесс %s уже запущен pid=%d\n ps=%s" % (command_module, task.pid, ps))
                return None
            task.pid = os.getpid()
            task.save()

        if task is not None:
            try:
                return func(*args, **kwargs)
            except Exception as e:
                log_error(e.message)
            finally:
                task.delete()

    return wrapper

# coding:utf-8


class AppException(Exception):

    def __init__(self, message=None):
        super(Exception, self).__init__()
        self.message = message

    def __repr__(self):
        return '{}'.format(self.message)

    def __str__(self):
        return str(self.message)

    def __unicode__(self):
        return self.__str__()

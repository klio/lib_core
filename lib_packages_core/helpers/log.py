import logging

from django.conf import settings


def get_logger(fn_log='error', logger_name=settings.LOGGER_DEFAULT):
    logger = logging.getLogger(logger_name)
    return getattr(logger, fn_log)


def get_logger_debug(logger_name=settings.LOGGER_DEFAULT):
    return get_logger('debug', logger_name)


def get_logger_error(logger_name=settings.LOGGER_DEFAULT):
    return get_logger('error', logger_name)


def get_logger_info(logger_name=settings.LOGGER_DEFAULT):
    return get_logger('info', logger_name)


def get_info_logger():
    logger = logging.getLogger(settings.LOGGER_DEFAULT)
    return getattr(logger, 'info')


log_debug = get_logger_debug()
log_error = get_logger_error()
log_info = get_logger_info()

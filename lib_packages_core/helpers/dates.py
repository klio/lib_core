# coding=utf-8
from django.utils import timezone


def date2str(date_, format_="%Y-%m-%d"):
    if not date_:
        return ''
    return date_.strftime(format_)


def datetime2str(datetime_, format_="%Y-%m-%dT%H:%M:%S"):
    return datetime_.strftime(format_)


def datetime_now():
    u""" Текущее время с таймзоной
    """
    return timezone.now()


def datetime_now_str():
    return datetime2str(datetime_now())


def seconds2strtime(seconds):
    hours = seconds / 3600
    seconds -= hours * 3600
    minutes = seconds / 60
    seconds -= minutes * 60
    return '%d:%02d:%02d' % (hours, minutes, seconds)
